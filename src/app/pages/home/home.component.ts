import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: [
  ]
})
export class HomeComponent implements OnInit {

  nombre:string = 'Nombre Usuario';
  constructor() { }

  ngOnInit(): void {
  }
  cambiarnombrehome(){
    this.nombre = 'Alex Medina Paredes';
  }

}
