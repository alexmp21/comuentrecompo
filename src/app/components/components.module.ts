import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { HijaComponent } from './hija/hija.component';



@NgModule({
  declarations: [NavbarComponent, HijaComponent],
  imports: [
    CommonModule
  ],
  exports:[
    NavbarComponent,
    HijaComponent
  ]
})
export class ComponentsModule { }
