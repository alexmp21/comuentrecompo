import { Component, Input, OnInit, Output,EventEmitter } from '@angular/core';


@Component({
  selector: 'app-hija',
  templateUrl: './hija.component.html',
  styles: [
  ]
})
export class HijaComponent implements OnInit {

  @Input() nombreHijo: string = 'Sin Nombre';
  @Output() cambioNombreHijo = new EventEmitter<string>();

  constructor() { }

  ngOnInit(){
  }

  cambiarnombre(){
    this.nombreHijo = 'Bernardo Peña Huaman';
    this.cambioNombreHijo.emit( this.nombreHijo)
  }
}
