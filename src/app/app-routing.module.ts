import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';

const router:Routes = [
  {path:'home', component:HomeComponent},
  {path:'**',pathMatch:'full', redirectTo:'home'}
]

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(router)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
